package lib;

import org.xtuml.bp.core.CorePlugin;

public class tSDT {
	public String name;
	public int memberA;
	public int memberB;
	public int memberC;
	
	public tSDT(String nm, int ma, int mb, int mc)
	{
		name = "init" + nm;
		memberA = ma;
		memberB = mb;
		memberC = mc;
		CorePlugin.out.println("construct tSDT");
	}
	public String getName()
	{
		CorePlugin.out.println("tSDT - getName");
		return name;
	}
	
	public int getMemberA()
	{
		return memberA;
	}
	public int getMemberB()
	{
		return memberB;
	}
	public int getMemberC()
	{
		return memberC;
	}
	
	public void setName(String val) {
		CorePlugin.out.println("tSDT - setName");
		name = val;
	}
	public void setMemberA(int val)
	{
		memberA = val;
	}
	public void setMemberB(int val)
	{
		memberB = val;
	}
	public void setMemberC(int val)
	{
		memberC = val;
	}
}
