//========================================================================

// Copyright 2007-2014 by Mentor Graphics Corp. All rights reserved.

//========================================================================

package lib;

import components.io.JsonWriter;
import components.io.JsonReader;
import org.xtuml.bp.core.CorePlugin;

public class JSON{



	public JSON(){
		CorePlugin.out.println("JSON Constructor");
	}
	

	public static tSDT decodeFromString(String data) {
		tSDT decodeData = new tSDT("a", 9, 8, 7);
		
		decodeData.name = "new name";
		String coreMsg = "test output";
		CorePlugin.out.println(coreMsg);
		CorePlugin.out.println(decodeData.name);
		return decodeData;

 }
	
	

	public static String encodeToString(tSDT data[]) {
		 String encodedData = "myTest"; 
		 
		 String coreMsg = "encodeToString" + ": " + encodedData;
			CorePlugin.out.println(coreMsg);
	       return encodedData;
	}

	
	public static int primitiveTypeDecode(String data) {
		
		CorePlugin.out.println(data);
		
		return 5;
	}

	public static String primitiveTypeEncode(int data) {
		
		//CorePlugin.out.println(data);
		String myReturn = "my return string B";
		return myReturn;
		
	}
	
}
